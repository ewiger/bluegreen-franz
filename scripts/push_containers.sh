#!/bin/bash
ACR_NAME=franz
ACR_IMAGE_PFX="$ACR_NAME.azurecr.io/"

# From command line arguments
IMAGE_NAME=${1:-webapp}
IMAGE_VERSION=${2:-latest}

IMAGE="${ACR_IMAGE_PFX}${IMAGE_NAME}:${IMAGE_VERSION}"

echo "Taging and pushing local image ${IMAGE_NAME}:${IMAGE_VERSION} to the remote repository -> $IMAGE"

# You can also log in with docker login
az acr login --name $ACR_NAME


# Tag local image with the remote alias
docker tag "$IMAGE_NAME:$IMAGE_VERSION" $IMAGE

# Push it to the remote
docker push $IMAGE