# Blue-Green "Franz"

> GOAL: use k8s configuration for network switching between blue and green infrastructure

Below we have some boiler plate configs for the blue-green deployment pattern often used in CI/CD of micro-service architecture. The following stack has been tested:

- Azure Kubernetes Engine (AKS)


## DevOps (k8s)

In this example we also look for configuration scenario to enable:

- Blue-Green deployment switching facilitated by kubernetes config files only
- Choice of the right ingress image with the load-balancing proxy to support TCP layer (L4)


### Prerequisites

You need to install all the latest versions of the following tools:

- Azure CLI
- kubectl (via Azure CLI)


### Basics

Get your credentials with

	az aks get-credentials --resource-group myResourceGroup --name myAKSCluster

Access kubernetes dashboard

	az aks browse --resource-group myResourceGroup --name myAKSCluster

Note: above might break with authorization errors. If cluster is freshly created, make sure to create the admin role with:

	kubectl create clusterrolebinding kubernetes-dashboard --clusterrole=cluster-admin --serviceaccount=kube-system:kubernetes-dashboard

> More information about AKS basics can be found [here](https://docs.microsoft.com/en-us/azure/aks/kubernetes-walkthrough) and [here](https://docs.bitnami.com/azure/get-started-aks/)

### Docker-compose (local environment)

Docker-compose is handy to build and test images locally. It can doing more sophisticated orchestration in the hosted environment. But, in a nutshell, it is just a wrapper around `docker` CLI with attached config `` (written in python).

It is handy to be used for local building, running and testing of the app images, etc. 

For our example we consider a simple web app to illustrate a switch for the blue-green deployment pattern.

Do 

	docker-compose up blue-webapp

and 

	docker-compose up green-webapp

to test that images are building and working locally.

On browsing `http://localhost:8888/` you can check that images report different versions.

### Push docker images

The above docker commands will trigger building if the images are missing. You cna also achieve this with:

	docker-compose build --force-rm green-webapp
	docker-compose build --force-rm blue-webapp

Next you want to tag and push your image to the remote container registry.
The following script comes handy to do that:

	./scripts/push_containers.sh webapp 1.0.0  # blue
	./scripts/push_containers.sh webapp 1.2.0  # green

Check image availability in ACR.


## K8s networking

### Blue Green switching

We create rules in external `LoadBalancer` provided by Azure that flows the ingress traffic into the k8s cluster.

Then the Service configuration entry wires the load balancer traffic in as well as has a manually created Endpoints (empty pod selector). 

The switching happens by changing the routed IP inside the manually created Endpoint.  See `k8s/switch/bg-endpoints.yaml`

A variation to this approach would be to specify *ExternalName* in `k8s/switch/bg-lb-svc.yaml`.

> You can access a service via it's DNS name (as mentioned by you): servicename.namespace.svc.cluster.local

> You can use that DNS name to reference it in another namespace via a local service:


	kind: Service
	apiVersion: v1
	metadata:
	  name: service-y
	  namespace: namespace-a
	spec:
	  type: ExternalName
	  externalName: service-x.namespace-b.svc.cluster.local
	  ports:
	  - port: 80

An important note is that if one treats multiple *namespaces* as multiple clusters instead and, at the same time, there is a routed network traffic between IPs (or DNS entries), then the whole approach to *use k8s configuration for network switching* between blue and green infrastructure works as well.

Also the above works for linking any external services into your cluster network.


## Usage

Create namespaces (clusters):

	kubectl apply -f k8s/app/blue.yaml
	kubectl apply -f k8s/app/green.yaml

The switch is in:

	kubectl apply -f k8s/switch/bg-lb-svc.yaml

And the endpoint configuration acts the flip-flop:

	kubectl apply -f k8s/switch/bg-endpoints.yaml


You can inspect settings with:

	kubectl describe svc bg-switch
	kubectl describe ep bg-switch
